/**
 * Alignment enum.
 */
package com.coldcore.gfx;

public enum Align {
  LEFT,
  RIGHT,
  TOP,
  BOTTOM,
  CENTER
}
