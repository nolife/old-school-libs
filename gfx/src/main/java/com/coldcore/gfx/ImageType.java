/**
 * Image type enum.
 */
package com.coldcore.gfx;

import java.awt.image.BufferedImage;

public enum ImageType {

  TYPE_INT_RGB(BufferedImage.TYPE_INT_RGB),
  TYPE_4BYTE_ABGR(BufferedImage.TYPE_4BYTE_ABGR);

  public int value;

  ImageType(int value) {
   this.value = value;
 }
}
