/**
 * PNG format writer.
 */
package com.coldcore.gfx;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class Png {

  /** Convert image data into PNG format
   * @param img Image
   * @param out Output
   */
  public static void write(BufferedImage img, OutputStream out) throws IOException {
    if (!Gfx.isValid(img)) return;
    ImageIO.write(img, "png", out);
    out.flush();
  }


  /** Convenience method
   * @see com.coldcore.gfx.Png#write(java.awt.image.BufferedImage, java.io.OutputStream)
   */
  public static byte[] write(BufferedImage img) throws IOException {
    if (!Gfx.isValid(img)) return null;
    ByteArrayOutputStream out = new ByteArrayOutputStream();
    write(img, out);
    return out.toByteArray();
  }

}
