/**
 * Created by IntelliJ IDEA.
 * User: sector
 * Date: Sep 26, 2004
 * Time: 1:41:45 AM
 */
package com.coldcore.pro.ftpsync;

public class Log {

  public void startTask(String description) {
    System.out.print("[TASK] "+description+" ... ");
  }


  public void midTask(String description) {
    System.out.print("["+description+"] - ");
  }


  public void endTask(String result) {
    System.out.println(result);
  }


  public void endTask(boolean result) {
    if (!result) endTask("FAILED");
    else         endTask("OK");
  }


  public void note(String message) {
    System.out.println("[NOTE] "+message);
  }

  
  public void error(Exception e) {
    System.out.println();
    System.out.println("[ERROR] "+e.toString());
    e.printStackTrace();
    System.out.println();
  }
}
