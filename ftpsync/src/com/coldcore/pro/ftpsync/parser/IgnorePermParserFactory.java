/**
 * Created by IntelliJ IDEA.
 * User: Sector
 * Date: Sep 27, 2004
 * Time: 1:00:46 PM
 */
package com.coldcore.pro.ftpsync.parser;

import org.apache.commons.net.ftp.FTPFileEntryParser;
import org.apache.commons.net.ftp.parser.FTPFileEntryParserFactory;

public class IgnorePermParserFactory implements FTPFileEntryParserFactory {

  public FTPFileEntryParser createFileEntryParser(String key) {
    return new IgnorePermParser();
  }
}