/**
 * Created by IntelliJ IDEA.
 * User: Sector
 * Date: Sep 27, 2004
 * Time: 12:59:03 PM
 */
package com.coldcore.pro.ftpsync.parser;

import org.apache.commons.net.ftp.parser.UnixFTPEntryParser;
import org.apache.commons.net.ftp.FTPFileEntryParser;
import org.apache.commons.net.ftp.FTPFile;

import java.util.StringTokenizer;

public class IgnorePermParser extends UnixFTPEntryParser implements FTPFileEntryParser {
  public FTPFile parseFTPEntry(String entry) {
    StringTokenizer st = new StringTokenizer(entry);
    String permissions = st.nextToken().charAt(0)+"rwxrwxrwx";
    return super.parseFTPEntry(permissions+entry.substring(10));
  }
}