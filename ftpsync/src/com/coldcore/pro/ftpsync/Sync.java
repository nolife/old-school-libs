/**
 * Created by IntelliJ IDEA.
 * User: Sector
 * Date: Sep 24, 2004
 * Time: 4:19:26 PM
 */
package com.coldcore.pro.ftpsync;

import com.coldcore.misc.CFile;
import com.coldcore.pro.ftpsync.parser.IgnorePermParserFactory;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.parser.FTPFileEntryParserFactory;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;

public class Sync {

  private FTPClient  ftpClient;
  private Properties props;
  private Log        log;

  public static final String KEY_LOCAL_DIR        = "local.dir";
  public static final String KEY_REMOTE_DIR       = "remote.dir";
  public static final String KEY_TRANSFER_MODE    = "transfer.mode";
  public static final String KEY_DELETE_REDUNDANT = "delete.redundant";
  public static final String KEY_OVERWRITE_SIZE   = "overwrite.size.key";
  public static final String KEY_OVERWRITE_OLD    = "overwrite.new.key";
  public static final String KEY_OVERWRITE_NEW    = "overwrite.old.key";
  public static final String KEY_OVERWRITE_ALL    = "overwrite.all.key";
  public static final String KEY_TIME_DISPERSION  = "time.dispersion";
  public static final String KEY_TIMEZONE_DIFF    = "timezone.difference";
  public static final String KEY_RECURSIVE_MODE   = "recursive.mode";
  public static final String KEY_USERNAME         = "username";
  public static final String KEY_PASSWORD         = "password";
  public static final String KEY_SERVER           = "server";
  public static final String KEY_PORT             = "port";
  public static final String KEY_PASV_MODE        = "pasv.mode";

  public static final String FTP_PARSER_FACTORY   = "ftp.parser.factory";

  public static final String MODE_UPLOAD   = "upload";
  public static final String MODE_DOWNLOAD = "download";


  public Sync() {
    ftpClient = new FTPClient();
    log       = new Log();
    createProps();
  }


  private void createProps() {
    props = new Properties();
    props.put(KEY_SERVER,           "my.unknown.server");
    props.put(KEY_PORT,             new Integer(21));
    props.put(KEY_USERNAME,         "my.unknown.username");
    props.put(KEY_PASSWORD,         "my.unknown.password");
    props.put(KEY_REMOTE_DIR,       "my.unknown.remote.dir");
    props.put(KEY_LOCAL_DIR,        "my.unknown.local.dir");
    props.put(KEY_DELETE_REDUNDANT, new Boolean(false));
    props.put(KEY_TRANSFER_MODE,    MODE_UPLOAD);
    props.put(KEY_TIME_DISPERSION,  new Integer(0));
    props.put(KEY_TIMEZONE_DIFF,    new Integer(0));
    props.put(KEY_OVERWRITE_ALL,    new Boolean(false));
    props.put(KEY_OVERWRITE_NEW,    new Boolean(false));
    props.put(KEY_OVERWRITE_OLD,    new Boolean(false));
    props.put(KEY_OVERWRITE_SIZE,   new Boolean(false));
    props.put(KEY_RECURSIVE_MODE,   new Boolean(false));
    props.put(KEY_PASV_MODE,        new Boolean(true));
  }


  private boolean proceedWithUploadMode(String remoteDir, String localDir) throws IOException {
    SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm");

    /* Get files and folders on FTP side and on client side.
     */
    log.startTask("Listing files in REMOTE directory '"+remoteDir+"'");
    ftpClient.changeWorkingDirectory(remoteDir);
    if (!ftpClient.printWorkingDirectory().equals(remoteDir)) {
      log.endTask("FAILED (DIR NOT FOUND)");
      return false;
    }

    FTPFile[] remList = ftpClient.listFiles();
    ArrayList a1 = new ArrayList();
    ArrayList a2 = new ArrayList();
    for (int j = 0; j < remList.length; j++) {
      if (remList[j].isFile()) a1.add(remList[j]);
      else                     a2.add(remList[j]);
    }
    FTPFile[] remFiles   = (FTPFile[]) a1.toArray(new FTPFile[a1.size()]);
    FTPFile[] remSubDirs = (FTPFile[]) a2.toArray(new FTPFile[a2.size()]);

    StringBuffer sb = new StringBuffer();
    for (int j = 0; j < remSubDirs.length; j++)
      sb.append("\n<DIR>  "+remSubDirs[j].getName());
    for (int j = 0; j < remFiles.length; j++)
      sb.append("\n<FILE> "+remFiles[j].getName());
    log.endTask(sb.toString()+"\n--- OK");

    log.startTask("Listing files in LOCAL directory '"+localDir+"'");
    CFile locList = new CFile(new File(localDir));
    if (!locList.getFile().exists()) {
      log.endTask("FAILED (DIR NOT FOUND)");
      return false;
    }
    File[] locFiles   = locList.list(true);
    File[] locSubDirs = locList.list(false);

    sb = new StringBuffer();
    for (int j = 0; j < locSubDirs.length; j++)
      sb.append("\n<DIR>  "+locSubDirs[j].getName());
    for (int j = 0; j < locFiles.length; j++)
      sb.append("\n<FILE> "+locFiles[j].getName());
    log.endTask(sb.toString()+"\n--- OK");

    /* Delete redundant files and folders from FTP side.
     */
    boolean delRedundant = ((Boolean) props.get(KEY_DELETE_REDUNDANT)).booleanValue();
    if (delRedundant) {
      log.note("Engaging scan for redundant REMOTE files and folders.");
      int deletedDirs  = 0;
      int deletedFiles = 0;

      for (int j = 0; j < remSubDirs.length; j++) {
        String remDir = remSubDirs[j].getName();
        log.startTask("Checking folder '"+remDir+"'");

        boolean delete = true;
          for (int z = 0; z < locSubDirs.length; z++) {
            String locDir = locSubDirs[z].getName();
            if (locDir.equals(remDir)) {
              delete = false;
              break;
            }
          }

        if (delete) log.endTask("DELETE (REDUNDANT)");
        else        log.endTask("KEEP (EXISTS)");

        if (delete) {
          log.startTask("Deleting redundant REMOTE folder '"+remDir+"'");
          boolean result = ftpClient.removeDirectory(remDir);
          log.endTask(result);
          if (!result) return false;
          deletedDirs++;
          remSubDirs[j] = null;
        }
      }

      for (int j = 0; j < remFiles.length; j++) {
        String remFile = remFiles[j].getName();
        log.startTask("Checking file '"+remFile+"'");

        boolean delete = true;
          for (int z = 0; z < locFiles.length; z++) {
            String locFile = locFiles[z].getName();
            if (locFile.equals(remFile)) {
              delete = false;
              break;
            }
          }

        if (delete) log.endTask("DELETE (REDUNDANT)");
        else        log.endTask("KEEP (EXISTS)");

        if (delete) {
          log.startTask("Deleting redundant REMOTE file '"+remFile+"'");
          boolean result = ftpClient.deleteFile(remFile);
          log.endTask(result);
          if (!result) return false;
          deletedFiles++;
          remFiles[j] = null;
        }
      }
      log.note("Deleted "+deletedFiles+" redundant files , "+deletedDirs+" folders.");
    }

    /* Upload files to FTP side based on upload settings.
     */
    log.note("Engaging LOCAL files upload.");
    long    timeDiff      = ((Integer) props.get(KEY_TIME_DISPERSION)).intValue()*60*1000;
    long    zoneDiff      = ((Integer) props.get(KEY_TIMEZONE_DIFF)).intValue()*60*60*1000;
    boolean overwriteAll  = ((Boolean) props.get(KEY_OVERWRITE_ALL)).booleanValue();
    boolean overwriteNew  = ((Boolean) props.get(KEY_OVERWRITE_NEW)).booleanValue();
    boolean overwriteOld  = ((Boolean) props.get(KEY_OVERWRITE_OLD)).booleanValue();
    boolean overwriteSize = ((Boolean) props.get(KEY_OVERWRITE_SIZE)).booleanValue();
    int uploaded = 0;

    for (int j = 0; j < locFiles.length; j++) {
      String locFile = locFiles[j].getName();
      log.startTask("Checking file '"+locFile+"'");

      boolean exists  = false;
      long    locTime = locFiles[j].lastModified();
      long    remTime = 0;
      long    locSize = locFiles[j].length();
      long    remSize = 0;
      for (int z = 0; z < remFiles.length; z++) {
        if (remFiles[z] == null) continue;
        String remFile = remFiles[z].getName();
        if (locFile.equals(remFile)) {
          remTime = remFiles[z].getTimestamp().getTime().getTime()+zoneDiff;
          remSize = remFiles[z].getSize();
          exists  = true;
          break;
        }
      }

      long diff = locTime-remTime;
      if (diff < 60*1000 && diff > -60*1000) diff = 0;
      String  locDate = sdf.format(new Date(locTime));
      String  remDate = sdf.format(new Date(remTime));
      boolean upload  = false;

      log.midTask("LOCAL "+locSize+"b "+locDate+ (exists ? ", REMOTE "+remSize+"b "+remDate : ""));

      if (overwriteAll) {
        log.endTask("UPLOAD (OVERWRITE ALL MODE)");
        upload = true;
      } else if (!exists) {
        log.endTask("UPLOAD (NEW FILE)");
        upload = true;
      } else if (locSize != remSize && overwriteSize) {
        log.endTask("UPLOAD (SIZE DIFFER MODE)");
        upload = true;
      } else if (diff < 0 && diff < -timeDiff) {
        if (overwriteNew) {
          log.endTask("UPLOAD (OVERWRITE NEW MODE)");
          upload = true;
        } else {
          log.endTask("SKIP (NO OVERWRITE NEW MODE)");
        }
      } else if (diff > 0 && diff > timeDiff) {
        if (overwriteOld) {
          log.endTask("UPLOAD (OVERWRITE OLD MODE)");
          upload = true;
        } else {
          log.endTask("SKIP (NO OVERWRITE OLD MODE)");
        }
      } else {
        log.endTask("SKIP (UP TO DATE)");
      }

      if (upload) {
        log.startTask("Uploading LOCAL file '"+locFile+"'");
        InputStream in = null;
        try {
          in              = new BufferedInputStream(new FileInputStream(locFiles[j]));
          boolean result  = ftpClient.storeFile(locFile, in);
          log.endTask(result);
          if (!result) return false;

          if (!locFiles[j].setLastModified(System.currentTimeMillis()))
            new CFile(locFiles[j]).truncateFile(locFiles[j].length());
        } finally {
          try { in.close(); } catch (Exception e) {}
        }
        uploaded++;
      }
    }
    log.note("Uploaded "+uploaded+" files.");

    /* Create folders on FTP side which exist only on client side.
     */
    log.note("Engaging creation of REMOTE folders.");
    int createdDirs = 0;

    for (int j = 0; j < locSubDirs.length; j++) {
      String locSubDir = locSubDirs[j].getName();
      log.startTask("Checking folder '"+locSubDir+"'");

      boolean create = true;
      for (int z = 0; z < remSubDirs.length; z++) {
        if (remSubDirs[z] == null) continue;
        String remSubDir = remSubDirs[z].getName();
        if (locSubDir.equals(remSubDir)) {
          create = false;
          break;
        }
      }

      if (create) log.endTask("CREATE (NEW FOLDER)");
      else        log.endTask("SKIP (EXISTS)");

      if (create) {
        log.startTask("Creating REMOTE folder '"+locSubDir+"'");
        boolean result = ftpClient.makeDirectory(locSubDir);
        log.endTask(result);
        if (!result) return false;
        createdDirs++;
      }
    }
    log.note("Created "+createdDirs+" folders.");

    /* Enter recursive mode for every folder.
     */
    boolean recursive = ((Boolean) props.get(KEY_RECURSIVE_MODE)).booleanValue();
    if (recursive) {
      for (int j = 0; j < locSubDirs.length; j++) {
        String locDir = locSubDirs[j].getName();
        log.note("Using RECURSIVE MODE for folder '"+locDir+"'");
        boolean result = proceedWithUploadMode(remoteDir+"/"+locDir, localDir+"/"+locDir);
        if (!result) return false;
      }
    }

    return true;
  }


  public boolean execute() throws Exception {
    FTPFileEntryParserFactory ftpParserFactory = (FTPFileEntryParserFactory) props.get(FTP_PARSER_FACTORY);
    if (ftpParserFactory != null) ftpClient.setParserFactory(ftpParserFactory);

    String  server    = props.getProperty(KEY_SERVER);
    int     port      = ((Integer) props.get(KEY_PORT)).intValue();
    String  user      = props.getProperty(KEY_USERNAME);
    String  pass      = props.getProperty(KEY_PASSWORD);
    String  remoteDir = props.getProperty(KEY_REMOTE_DIR);
    String  localDir  = props.getProperty(KEY_LOCAL_DIR);
    String  transMode = props.getProperty(KEY_TRANSFER_MODE);
    boolean connected = false;
    boolean errors    = true;
    boolean result;
    try {
      log.note("STARTING SYNCHRONIZATION");

      log.startTask("Connecting to '"+server+":"+port+"' as '"+user+"'");
      ftpClient.connect(server, port);
      connected = true;
      result    = ftpClient.login(user, pass);
      log.endTask(result);
      if (!result) return false;

      ftpClient.setFileType(FTPClient.BINARY_FILE_TYPE);

      boolean passive = ((Boolean) props.get(KEY_PASV_MODE)).booleanValue();
      if (passive) {
        log.note("Switching to passive mode.");
        ftpClient.enterLocalPassiveMode();
      }

      if (transMode.equals(MODE_UPLOAD)) {
        log.note("Engaging UPLOAD mode.");
        result = proceedWithUploadMode(remoteDir, localDir);
        if (!result) log.note("UPLOAD has FAILED.");
        else         log.note("UPLOAD has been completed.");
        if (!result) return false;
      } else {
        log.note("Sorry, DOWNLOAD mode is not yet supported.");
        return false;
      }

      errors = false;
    } catch (Exception e) {
      log.error(e);
      throw e;
    } finally {
      if (connected)
        try {
          log.startTask("Disconnecting");
          ftpClient.disconnect();
          log.endTask(true);
        } catch (Exception e) {}

      log.note("SYNCHRONIZATION COMPLETE ("+(errors ? "WITH ERRORS" : "NO ERRORS")+")");
    }

    return true;
  }


  public Properties getProps() {
    return props;
  }


  public static void main(String[] args) {
    System.out.println();
    System.out.println("=============================");
    System.out.println("SyncFTP by ColdCore.com v1.05");
    System.out.println("=============================");
    System.out.println();

    Sync sync = new Sync();

    try {
      if (args.length < 6) throw new Exception();

      String command   = args[0];
      String server    = args[1];
      String username  = args[2];
      String password  = args[3];
      String localDir  = args[4];
      String remoteDir = args[5];

      if (!command.equals("up") && !command.equals("down")) {
        System.out.println("Unknown command.");
        System.out.println();
        throw new Exception();
      }

      if (server.indexOf(".") == -1) {
        System.out.println("Server syntax error.");
        System.out.println();
        throw new Exception();
      }

      sync.getProps().put(KEY_TRANSFER_MODE, command.equals("up") ? MODE_UPLOAD : MODE_DOWNLOAD);
      sync.getProps().put(KEY_SERVER,        server);
      sync.getProps().put(KEY_USERNAME,      username);
      sync.getProps().put(KEY_PASSWORD,      password);
      sync.getProps().put(KEY_LOCAL_DIR,     localDir);
      sync.getProps().put(KEY_REMOTE_DIR,    remoteDir);

      for (int j = 6; j < args.length; j++) {
        String arg = args[j];
             if (arg.equals("-p"))  sync.getProps().put(KEY_PORT,             new Integer(args[++j]));
        else if (arg.equals("-tz")) sync.getProps().put(KEY_TIMEZONE_DIFF,    new Integer(args[++j]));
        else if (arg.equals("-td")) sync.getProps().put(KEY_TIME_DISPERSION,  new Integer(args[++j]));
        else if (arg.equals("-rc")) sync.getProps().put(KEY_RECURSIVE_MODE,   new Boolean(true));
        else if (arg.equals("-dr")) sync.getProps().put(KEY_DELETE_REDUNDANT, new Boolean(true));
        else if (arg.equals("-os")) sync.getProps().put(KEY_OVERWRITE_SIZE,   new Boolean(true));
        else if (arg.equals("-on")) sync.getProps().put(KEY_OVERWRITE_NEW,    new Boolean(true));
        else if (arg.equals("-oo")) sync.getProps().put(KEY_OVERWRITE_OLD,    new Boolean(true));
        else if (arg.equals("-oa")) sync.getProps().put(KEY_OVERWRITE_ALL,    new Boolean(true));
        else if (arg.equals("-ip")) sync.getProps().put(FTP_PARSER_FACTORY,   new IgnorePermParserFactory());
        else if (arg.equals("-np")) sync.getProps().put(KEY_PASV_MODE,        new Boolean(false));
        else {
          System.out.println("Unknown option: "+arg);
          System.out.println();
          throw new Exception();
        }
      }

    } catch (Exception e) {
      System.out.println("Use:");
      System.out.println("  Sync command server username password localDir remoteDir [options]");
      System.out.println();
      System.out.println("Commnds:");
      System.out.println("  up     Upload files and folders from local dir to remote dir.");
      System.out.println("  down   Download files and folders from remote dir to local dir.");
      System.out.println();
      System.out.println("Options:");
      System.out.println("  -p port      Use different FTP port number (default 21).");
      System.out.println("  -tz hours    Time zone difference in hours on a server, negative or positive (default 0).");
      System.out.println("  -td minutes  File timestamp dispersion in minutes (default 0).");
      System.out.println("  -rc          Recursive, include all sub directories (default no).");
      System.out.println("  -dr          Delete redundant files and folders, which exist only in target dir (default no).");
      System.out.println("  -os          Overwrite file if the size does not match (default no).");
      System.out.println("  -on          Overwrite new files (default no).");
      System.out.println("  -oo          Overwrite old files (default no).");
      System.out.println("  -oa          Overwrite all files (default no).");
      System.out.println("  -ip          Ignore file permossions.");
      System.out.println("  -np          Do not use passive mode (dafault no).");
      System.out.println();
      System.out.println("Example:");
      System.out.println("  Sync up java.ee myName myPass c:/myfiles /tmp/files -on -oo -os -tz -3 -td 15 -rc");
      System.out.println();
      System.exit(1);
    }

    try {
      boolean result = sync.execute();
      if (!result) throw new Exception();
    } catch (Exception e) {
      System.exit(1);
    }
  }
}
