/**
 * This class is used to work with InputStream/OutputStream as it was a DataSOurce object.
 */
package com.coldcore.email;

import javax.activation.DataSource;
import java.io.InputStream;
import java.io.OutputStream;

public class DataSourceStream implements DataSource {

  private InputStream  inputStream;
  private OutputStream outputStream;
  private String contentType;


  public DataSourceStream(InputStream inputStream, OutputStream outputStream, String contentType) {
    this.inputStream  = inputStream;
    this.outputStream = outputStream;
    this.contentType  = contentType;
  }


  public String getName() {
    return "DataSourceStream";
  }


  public InputStream getInputStream() {
    return inputStream;
  }


  public OutputStream getOutputStream() {
    return outputStream;
  }


  public String getContentType() {
    return contentType;
  }
}
