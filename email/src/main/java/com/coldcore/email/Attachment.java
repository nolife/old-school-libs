/**
 * Attachment class of WMessage object.
 */
package com.coldcore.email;

public class Attachment {

  private String filename; //Filename (not encoded, may contain whatever characters)
  private byte[] data;     //File data (not encoded)


  public Attachment(String filename, byte[] data) {
    this.filename = filename;
    this.data = data;
  }


  /** Filename (decoded, may contain whatever characters) */
  public String getFilename() {
    return filename;
  }


  /** File data */
  public byte[] getData() {
    return data;
  }
}