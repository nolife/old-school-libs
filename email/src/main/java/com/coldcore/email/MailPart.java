/**
 * This class performs operations with parts of multipart mails.
 */

package com.coldcore.email;

import javax.mail.MessagingException;
import javax.mail.Part;
import javax.mail.internet.MimeUtility;
import java.util.StringTokenizer;

public class MailPart {

  private MailPart() {}


  /** @return Lowercased content type of the part. */
  public static String getContentType(Part part) throws MessagingException {
    String contentType = part.getContentType();

    //If more the one token then use only first one
    StringTokenizer tokenizer = new StringTokenizer(contentType);
    if (tokenizer.hasMoreTokens()) contentType = tokenizer.nextToken();

    //Remove ; in the end if exists
    int length = contentType.length();
    if (contentType.endsWith(";")) contentType = contentType.substring(0, length - 1);

    return contentType.toLowerCase();
  }


  /** @return Decoded filename associated with this part or null. */
  public static String getFilename(Part part) throws MessagingException {
    String filename = "";

    //Try to get filename from filename = (if exists)
    filename = part.getFileName();

    //Try to get filename from Content-Type (for POP3 protocol) (if previous step failed)
    if (filename.equals(""))
      try {
        String contentType = part.getContentType();

        if (contentType.indexOf("name") != -1) {
          //Extract filename from name = "xxx" or name="xxx"
          int start = contentType.indexOf("name");
          start = contentType.indexOf("\"", start) + 1;
          int end = contentType.indexOf("\"", start + 1);
          filename = contentType.substring(start, end);

          try { //Decoding may fail, do not care
            filename = MimeUtility.decodeText(filename);
          } catch (Exception e){}
        }
      } catch (Exception e) {}

    if (!filename.equals("")) return filename;
    return null;
  }
}
