/**
 * This class performs e-email address encoding to email-safe format.
 */

package com.coldcore.email;

import com.coldcore.misc5.StringReaper;

import javax.mail.internet.MimeUtility;
import java.io.UnsupportedEncodingException;

public class AddressEncoder {

  private AddressEncoder() {}


  /** Encodes e-email adderss (name only).
   *  NOTE: Base64 and UTF-8. In a name any symbols allowed.
   *  Sector <sector@hot.ee> -> =?UTF-8?B?SGkgdGhlcmXRgA==?= <sector@hot.ee>
   */
  public static String encode(String address) throws UnsupportedEncodingException {
    /* Address may contain name and e-email or only e-email. Last token is an e-email, all
     * before it must be name.
     */
    String name = "";
    StringReaper reaper = new StringReaper(address);
    String[] values = reaper.getValues(null, " ");
    for (int z = 0; z < values.length-1; z++)
      name = name + values[z] + " ";
    name = name.trim();
    String email = values[values.length-1];

    //Encode name if it is not empty
    if (!name.equals("")) name = MimeUtility.encodeText(name, "UTF-8", "B") + " ";

    return name + email;
  }


  /** Decodes e-email address (name and removes " symbols around it) */
  public static String decode(String address) throws UnsupportedEncodingException {
    /* NOTE: Sometime encoded name has " symbols around it, before decoding we must
     * remove them. First we will split address into name and e-email.
     */
    String name = "";
    StringReaper reaper = new StringReaper(address);
    String[] values = reaper.getValues(null, " ");
    for (int z = 0; z < values.length-1; z++)
      name = name + values[z] + " ";
    name = name.trim();
    String email = values[values.length-1];

    //Remove surrounding " symbol if needed. Then decode name.
    if (name.startsWith("\"") && name.endsWith("\"")) name = name.substring(1, name.length()-1);
    name = MimeUtility.decodeText(name);

    //Decoded name may als have surrounding " symbol (or ').
    if (name.startsWith("\"") && name.endsWith("\"")) name = name.substring(1, name.length()-1);
    if (name.startsWith("'") && name.endsWith("'")) name = name.substring(1, name.length()-1);

    //Some times name contains "\\(E-email\\)" string, get rid of it.
    reaper.setContent(name);
    reaper.replace("\\\\(E-email\\\\)", "");
    name = reaper.getContent().trim();

    if (!name.equals("")) name += " ";
    return name + email;
  }
}
