/**
 * Message for WebMail class.
 * This class stores email Message and contains methods for data extracting from Message class.
 * NOTE: This class is used only for email reading, not for sending.
 */
package com.coldcore.email;

import com.coldcore.misc5.Html;

import javax.mail.Address;
import javax.mail.Flags;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.MimeUtility;
import java.util.ArrayList;
import java.util.List;

public class InMessage {

  private Message message;  //Mail message
  private int number;       //Message number on email server
  private Reader.Protocol protocol;  //Protocol that this message was received through
  private String plainText; //Message plain text
  private String htmlText;  //Message HTML text
  private List<Attachment> attachments; //Attachment filenames


  public InMessage(int number, Message message, Reader.Protocol protocol) {
    this.message = message;
    this.number = number;
    this.protocol = protocol;
    attachments = new ArrayList<Attachment>();
  }


  /** @return Message object */
  public Message getMessage() {
    return message;
  }


  /** @return Message number in IMAP server */
  public int getNumber() {
    return number;
  }


  /** Extract message text.
   *  NOTE: If message does not have plain text then HTML text will be converted into plain text
   * @return Plain text or an empty string.
   */
  public String getPlainText() {
    if (plainText != null) return plainText;
    if (htmlText != null) return Html.htmlToPlain(htmlText);
    return "";
  }


  public void setPlainText(String plainText) {
    this.plainText = plainText;
  }


  /** @return Plain text or null */
  public String getRealPlainText() {
    return plainText;
  }


  /** Extract message text.
   *  NOTE: If message does not have HTML text then plain text will be converted into HTML text
   * @return HTML text or an empty string.
   */
  public String getHtmlText() {
    if (htmlText != null) return htmlText;
    if (plainText != null) return Html.convertPlain(plainText);
    return "";
  }


  /** @return HTML text or null */
  public String getRealHtmlText() {
    return htmlText;
  }


  public void setHtmlText(String htmlText) {
    this.htmlText = htmlText;
  }


  /** @return Decoded message subject, neveer null */
  public String getSubject() throws MessagingException {
    String subject = message.getSubject();

    try { //Decoding may fail, do not care!
      subject = MimeUtility.decodeText(subject);
    } catch (Exception e) {}

    return subject == null ? "" : subject;
  }


  /** @return Decoded message "from" or "reply to", never null */
  public String getSender() throws Exception {
    String sender = "";
    Address[] addresses = message.getFrom();
    if (addresses == null || addresses.length == 0) addresses = message.getReplyTo();
    if (addresses != null && addresses.length > 0) sender = addresses[0].toString();

    try { //Decoding may fail, do not care!
      sender = AddressEncoder.decode(sender);
    } catch (Exception e) {}

    return sender;
  }


  /** @return Decoded "to" and "cc" message recipients, never null */
  public List getRecipients() throws Exception {
    List<String> to = getRecipients(Message.RecipientType.TO);
    List<String> cc = getRecipients(Message.RecipientType.CC);
    to.addAll(cc);
    return to;
  }


  /** @return Decoded message recipients of given type, never null */
  public List<String> getRecipients(Message.RecipientType type) throws Exception {
    List<String> list = new ArrayList<String>();
    Address[] addresses = message.getRecipients(type);

    if (addresses != null && addresses.length > 0)
      for (Address address : addresses) {
        String str = address.toString();

        try { //Decoding may fail, do not care!
          str = AddressEncoder.decode(str);
        } catch (Exception e) {}

        list.add(str);
      }

    return list;
  }


  /** @return Message size */
  public int getSize() throws Exception {
    int size = message.getSize();
    //If message size is less then 1 Kb, POP3 protocol returns message size incorrectly
    if (protocol == Reader.Protocol.POP3 && size < 1024 || size == -1) size = 1024;
    return size;
  }


  /** @return Readable message size */
  public String getReadableSize() throws Exception {
    int size = getSize();
    if (size >= 1024*1024) return size/(1024*1024) + " MB";
    else if (size >= 1024) return size/1024 + " KB";
    else return size + " b";
  }


  /** @return TRUE if message is marked as new (imap feature, pop3 will always return false) */
  public boolean isNew() throws Exception {
    if (protocol == Reader.Protocol.POP3) return false;
    return !message.isSet(Flags.Flag.SEEN);
  }


  /** @return Attachments, never null */
  public List<Attachment> getAttachments() {
    return attachments;
  }


  /** Guess if the message has attachments based on its content type (not always correct)
   * @return Whether the massage has attachments
   */
  public boolean hasAttachments() throws Exception {
    return message.isMimeType("multipart/mixed");
  }
}