/**
 * IMAP or POP3 client.
 */

package com.coldcore.email;

import com.coldcore.misc5.CByte;

import javax.activation.DataHandler;
import javax.mail.*;
import javax.mail.internet.MimeUtility;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.channels.Channels;
import java.util.*;

public class Reader implements HttpSessionBindingListener {

  private Session session;        //Current email session and store
  private Store store;            //
  private String host;            //Mail server host name or IP address
  private String login;           //Account data
  private String password;        //
  private Protocol protocol;      //Read protocol
  private Folder curFolder;       //Current folder (first is INBOX for both protocols)
  private Folder[] folders;       //All folders (pop3 has only INBOX)
  private InMessage curMessage;   //Current message
  private boolean doAttachments;  //If TRUE then message processing will involve extractment of attachments


  public enum Protocol {
    IMAP("imap"),
    POP3("pop3");

    public String protocol;

    Protocol(String protocol) {
     this.protocol = protocol;
   }
  }


  public Reader() {
    host = "127.0.0.1";
    protocol = Protocol.IMAP;
    Properties properties = new Properties(System.getProperties());
    session = Session.getInstance(properties, null);
  }


  public void setHost(String host) {
    if (isConnected()) throw new IllegalStateException("Disconnect first");
    this.host = host;
  }


  public String getHost() {
    return host;
  }


  public Folder[] getAllFolders() {
    return folders;
  }


  public void setLogin(String login) {
    if (isConnected()) throw new IllegalStateException("Disconnect first");
    this.login = login;
  }


  public String getLogin() {
    return login;
  }


  public void setPassword(String password) {
    if (isConnected()) throw new IllegalStateException("Disconnect first");
    this.password = password;
  }


  public String getPassword() {
    return password;
  }


  public void setProtocol(Protocol protocol) {
    if (isConnected()) throw new IllegalStateException("Disconnect first");
    this.protocol = protocol;
  }


  public Protocol getProtocol() {
    return protocol;
  }


  /** @return TRUE if server is still replying */
  public boolean isConnected() {
    try {
      Folder inbox = findFolder("INBOX");
      if (inbox.isOpen()) return true;
    } catch (Exception e) {}
    return false;
  }


  /** Connect to the server */
  public void connect() throws MessagingException {
    connect(-1);
  }


  /** Connect to the server
   * @param port Port to connect to
   */
  public void connect(int port) throws MessagingException {
    if (isConnected()) return;

    store = session.getStore(protocol.protocol);
    store.connect(host, port, login, password);

    //Cache all folders and set current one to INBOX
    cacheFolders();
    setFolder("INBOX");
  }


  /** Set current folder
   * @param folderName Folder name
   */
  public void setFolder(String folderName) throws MessagingException {
    /* Get requested folder and open it (if not already opened).
     * NOTE: We will open folder only if it is a messages folder.
     */
    curFolder = findFolder(folderName);
    if (!curFolder.isOpen() && curFolder.exists()
    && ((curFolder.getType() & Folder.HOLDS_MESSAGES) != 0)) curFolder.open(Folder.READ_WRITE);
  }


  /** @return Current folder */
  public Folder getCurrentFolder() {
    return curFolder;
  }


  /** Get message.
   *  NOTE: Very first message number is 1.
   *  NOTE: If "process" is false then only message headers will be set, otherwise text and attachment filenames will also be processed
   *  NOTE: If "doAttachments" is true then attachments data will be extracted
   */
  public InMessage getMessage(int number, boolean process, boolean doAttachments) throws MessagingException, IOException {
    this.doAttachments = doAttachments;
    Message message = curFolder.getMessage(number);
    curMessage = new InMessage(number, message, protocol);
    if (process) processMessage();
    return curMessage;
  }


  /** Closes connection to the server */
  public void close() {
    try { //Close all open folders
      if (folders != null)
        for (Folder folder : folders)
          if (folder.isOpen()) folder.close(true);
    } catch (Exception e) {}

    try { //Close the store
      if (store != null) store.close();
    } catch (Exception e) {}
  }


  /** Refresh the current folder */
  public void refreshFolder() throws MessagingException {
    if (protocol == Protocol.POP3) {
      //For pop3 we need to close the folder and then reopen it
      curFolder.close(true);
      curFolder.open(Folder.READ_WRITE);
    } else {
      //For imap we need to call "expunge" method
      curFolder.expunge();
    }
  }


  /** @return Amount of new messages in the current folder (0 for pop3) */
  public int getNewMessageCount() throws MessagingException {
    if (protocol == Protocol.POP3) return 0;
    return curFolder.getNewMessageCount();
  }


  /** Delete messages in the current folder
   * @param numbers Message numbers
   */
  public void deleteMessages(int... numbers) throws MessagingException {
    if (numbers == null) return;

    /* Get all requested messages and set DELETED flag to them, then refresh
    * current folder and DELETED messages will be gone.
    */
    Message[] messages = findMessages(numbers);
    curFolder.setFlags(messages, new Flags(Flags.Flag.DELETED), true);
    refreshFolder();
  }


  /** Copy messages from the current folder into another (imap only)
   * @param numbers Message numbers
   * @param folder Target folder
   */
  public void copyMessages(String folder, int... numbers) throws MessagingException {
    if (protocol == Protocol.POP3) return;

    //Get all requested messages and destination folder, then copy
    Message[] messages = findMessages(numbers);
    Folder target = findFolder(folder);
    curFolder.copyMessages(messages, target);
  }


  /** Copy messages from the current folder into another (imap only)
   * @param numbers Message numbers
   * @param folder Target folder
   */
  public void moveMessages(String folder, int... numbers) throws MessagingException {
    if (protocol == Protocol.POP3) return;

    //Copy messages into new folder then delete them
    copyMessages(folder, numbers);
    deleteMessages(numbers);
  }


  /** Find messages in the vurrent folder
   * @param numbers Message numbers
   * @return Messages or null
   */
  private Message[] findMessages(int... numbers) throws MessagingException {
    if (numbers == null) return null;
    List<Message> list = new ArrayList<Message>();
    for (int number : numbers) {
      list.add(curFolder.getMessage(number));
    }
    return list.toArray(new Message[0]);
  }


  /** @return Children folders (sorted and recursive) */
  private List<Folder> getAllClildren(Folder folder) throws MessagingException {
    List<Folder> list = new ArrayList<Folder>();

    //Get all children folders
    Folder[] childFolders = folder.list();
    for (Folder child : childFolders) {
      //Get folder name an add folder to vector
      String folderName = child.getName();
      String fullName = child.getFullName();
      //Ignore INBOX and hidden folders
      if (!folderName.startsWith(".") && !fullName.equalsIgnoreCase("INBOX")) list.add(child);
    }

    Collections.sort(list, new Comparator() {
      public int compare(Object o1, Object o2) {
        return ((Folder)o1).getName().compareToIgnoreCase(((Folder)o2).getName());
      }
    });

    //Get children of all folders (recursive)
    if (list.size() > 0)
      for (Folder f : list)
        list.addAll(getAllClildren(f));

    return list;
  }


  /** Fill "folders" array
   *  Note: If any folder has child folders they will be added as folder.child, folder.child.child etc.,
   *  "." is used as example, real hierarcy delimeter will be used.
   *  Note: INBOX will be first in array and hidden folders will not be added
   */
  private void cacheFolders() throws MessagingException {
    //Get root folder and all it's children folders
    Folder defaultFolder = store.getDefaultFolder();

    //Get all children folders
    List<Folder> list = getAllClildren(defaultFolder);

    //Add INBOX as a first folder in the list
    Folder inbox = defaultFolder.getFolder("INBOX");
    if (inbox.exists()) list.add(0, inbox);

    folders = list.toArray(new Folder[list.size()]);
  }


  /** Look for folder in the folders array
   * @return Folder or null
   */
  private Folder findFolder(String folderName) {
    for (Folder folder : folders)
      if (folder.getName().equals(folderName)) return folder;
    return null;
  }


  /** Close store when http session ends */
  public void valueUnbound(HttpSessionBindingEvent p1) {
    //Close store when http session ends
    close();
  }


  public void valueBound(HttpSessionBindingEvent p1) {}


  /** Processe current message */
  private void processMessage() throws MessagingException, IOException {
//    //DEBUG: Save message to file
//    OutputStream os = new FileOutputStream("C:\\temp\\mytest2.txt");
//    currentMessage.writeTo(os);

    Part p = curMessage.getMessage();
    String contentType = MailPart.getContentType(p);
    Object content = p.getContent();

    if (contentType.startsWith("text/html")) curMessage.setHtmlText((String) content);
    else if (contentType.startsWith("text/")) curMessage.setPlainText((String) content);
    else if (contentType.startsWith("multipart/")) {
      //Message is a multipart message, process it as multipart
      try {
        processMultipart(p);
      } catch (Exception ex) {
        //Exception - show entire message content
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        curMessage.getMessage().writeTo(os);
        curMessage.setPlainText(new String(os.toByteArray()));
      }
    } else processAttachmentPart(p); //Unknown type - possible no text and only one attachment
  }


  /** Processe multipart Part object. */
  private void processMultipart(Part part) throws MessagingException, IOException {
    Multipart multipart = (Multipart) part.getContent();
    int count = multipart.getCount();

    for (int loop = 0; loop < count; loop++) {
      Part p = multipart.getBodyPart(loop);
      String contentType = MailPart.getContentType(p);
      Object content     = p.getContent();

      if (contentType.startsWith("text/html")) {
        //Html format
        if (curMessage.getRealHtmlText() == null) curMessage.setHtmlText(content.toString());
      } else if (contentType.startsWith("text/")) {
        //Plain text format
        if (curMessage.getRealPlainText() == null) curMessage.setPlainText(content.toString());
      } else if (contentType.startsWith("multipart/")) {
        //This part is a multipart, process it (recursive).
        processMultipart(p);
      } else {
        //Unknown type - possible attachment, process it
        processAttachmentPart(p);
      }
    }
  }


  /** Processes attachment part */
  private void processAttachmentPart(Part part) throws MessagingException, IOException {
    String filename = MailPart.getFilename(part);

    if (filename != null) {
      byte[] data = null;

      //If we need to extract attachment data then do so
      if (doAttachments) {
        DataHandler dh = part.getDataHandler();
        InputStream in = dh.getInputStream();
        data = CByte.toByteArray(Channels.newChannel(in));
      }

      try { //Decoding may fail, do not care
        filename = MimeUtility.decodeText(filename);
      } catch (Exception e) {}

      //Add attachment (with or without file data)
      Attachment attachment = new Attachment(filename, data);
      curMessage.getAttachments().add(attachment);
    }
  }
}
