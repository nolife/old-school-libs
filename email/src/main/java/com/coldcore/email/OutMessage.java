/**
 * Message for sending via WebMail class. This class can create Message objcect that can be send via SMTP.
 */
package com.coldcore.email;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.*;
import javax.mail.internet.*;
import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;
import java.util.*;

public class OutMessage {

  private String htmlText;              //Message HTML text
  private String subject;               //Message subject
  private String from;                  //From
  private List<String> to;              //Recipients: to, cc, bcc
  private List<String> cc;              //
  private List<String> bcc;             //
  private List<Attachment> attachments; //Attachments
  private Map<String,String> headers;   //Message headers (key: name, value: value)


  public enum RecipientType {
    TO, CC, BCC
  }


  public OutMessage() {
    htmlText= "";
    subject = "";
    from = "";
    to = new ArrayList<String>();
    cc = new ArrayList<String>();
    bcc = new ArrayList<String>();
    attachments = new ArrayList<Attachment>();
    headers = new HashMap<String,String>();
  }


  public void setFrom(String from) throws AddressException, UnsupportedEncodingException {
    if (from == null) return;
    from = InternetAddress.parse(from)[0].toString();
    from = AddressEncoder.encode(from);
    this.from = from;
  }


  public void setSubject(String subject) {
    if (subject == null) return;
    try { //Encode subject, encoding may fail, do not care
      subject = MimeUtility.encodeText(subject, "UTF-8", "B");
    } catch (Exception e) {}
    this.subject = subject;
  }


  /** Add recipient
   * @param recipient Recipient
   * @param type Type
   */
  public void addRecipient(String recipient, RecipientType type) throws AddressException, UnsupportedEncodingException {
    if (recipient == null) return;
    recipient = InternetAddress.parse(recipient)[0].toString();
    recipient = AddressEncoder.encode(recipient);
    switch (type) {
      case TO: to.add(recipient); break;
      case CC: cc.add(recipient); break;
      default: bcc.add(recipient);
    }
  }


  /** Add coma separated recipients
   * @param recipients Recipients
   * @param type Type
   */
  public void addRecipients(String recipients, RecipientType type) throws AddressException, UnsupportedEncodingException {
    if (recipients == null) return;
    InternetAddress[] addresses = InternetAddress.parse(recipients);
    for (InternetAddress address : addresses)
      addRecipient(address.toString(), type);
  }


  /** @return Encoded recipients of specified type, never null */
  private String getRecipients(RecipientType type) {
    String str = "";
    List<String> list;
    switch (type) {
      case TO: list = to; break;
      case CC: list = cc; break;
      default: list = bcc;
    }

    Iterator it = list.iterator();
    while (it.hasNext()) {
      str += it.next();
      if (it.hasNext()) str += ", ";
    }
    return str;
  }


  public void setHtmlText(String htmlText) {
    if (htmlText == null) return;
    this.htmlText = htmlText;
  }


  /** Add attachment
   * @param attachment Attachment
   */
  public void addAttachment(Attachment attachment) {
    if (attachment == null) return;
    attachments.add(attachment);
  }


  /** Add message header
   * @param name Header name
   * @param value Header value
   */
  public void addHeader(String name, String value) {
    if (name == null || value == null) return;
    headers.put(name, value);
  }


  /** Create encoded BodyPart with inside
   * @param attachment Attachment
   * @return Body part
   */
  private MimeBodyPart createAttachmentPart(Attachment attachment) throws MessagingException {
    String filename = attachment.getFilename();
    try { //Encode filename, encoding may fail, do not care
      filename = MimeUtility.encodeText(filename, "UTF-8", "B");
    } catch (Exception e) {}

    //Set part content
    MimeBodyPart part = new MimeBodyPart();
    DataSource ds = new DataSourceStream(new ByteArrayInputStream(attachment.getData()), null, "application/octet-stream");
    part.setDataHandler(new DataHandler(ds));

    //Set part filename and also set correct part headers
    part.setFileName(filename);
    part.setHeader("Content-Type", "application/octet-stream; name=\""+filename+"\"");
    part.setHeader("Content-Transfer-Encoding", "base64"); //This will automaticaly encode content
    return part;
  }


  /** Create ready-to-send massage
   * @param session Session
   * @return Message
   */
  public Message createMessage(Session session) throws MessagingException {
    MimeMessage message = new MimeMessage(session);
    message.setFrom(new InternetAddress(from));
    message.setSubject(subject);
    message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(getRecipients(RecipientType.TO)));
    message.setRecipients(Message.RecipientType.CC, InternetAddress.parse(getRecipients(RecipientType.CC)));
    message.setRecipients(Message.RecipientType.BCC, InternetAddress.parse(getRecipients(RecipientType.BCC)));

    //If no attachments then only add text, otherwise make message multipart.
    if (attachments.size() > 0) {
      Multipart multipart = new MimeMultipart();

      //First attach message text as HTML text
      MimeBodyPart  textPart  = new MimeBodyPart();
      textPart.setText(htmlText, "utf-8");
      textPart.setHeader("Content-Type","text/html; charset=\"utf-8\"");
      textPart.setHeader("Content-Transfer-Encoding", "quoted-printable");
      multipart.addBodyPart(textPart);

      //Add all attachments
      for (Attachment attachment : attachments) {
        BodyPart part = createAttachmentPart(attachment);
        multipart.addBodyPart(part);
      }

      //Multipart now contain text and all attachments, add it to message
      message.setContent(multipart);
    } else {
      //No attachments, add only text
      message.setText(htmlText, "utf-8");
      message.setHeader("Content-Type","text/html; charset=\"utf-8\"");
      message.setHeader("Content-Transfer-Encoding", "quoted-printable");
    }

    //Set message date (optional), some servers set this automaticaly but some do not
    message.setSentDate(new Date());

    //Add additional message headers
    for (String key : headers.keySet()) {
      String value = headers.get(key);
      message.setHeader(key, value);
    }

    return message;
  }
}