/**
 * SMTP client.
 */
package com.coldcore.email;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.MessagingException;
import java.util.Properties;

public class Sender {

  private Session session;
  private String host;
  private int port;


  public Sender() {
    Properties properties = new Properties(System.getProperties());
    properties.put("email.transport.protocol", "smtp");
    session = Session.getInstance(properties, null);
    setHost("127.0.0.1");
    setPort(25);
  }


  /** Set host name or IP address
   * @param host Host name or IP
   */
  public void setHost(String host) {
    this.host = host;
    Properties properties = session.getProperties();
    properties.put("mail.smtp.host", host);
  }


  public String getHost() {
    return host;
  }


  public int getPort() {
    return port;
  }


  /** Set port
   * @param port Port
   */
  public void setPort(int port) {
    this.port = port;
    Properties properties = session.getProperties();
    properties.put("email.smtp.port", ""+port);
  }


  /** Send message
   * @param outMessage Message
   */
  public void sendMessage(OutMessage outMessage) throws MessagingException {
    Message message = outMessage.createMessage(session);
    Transport.send(message);
  }
}
