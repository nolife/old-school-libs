package com.coldcore.misc.clj;

import static com.coldcore.misc.clj.Clojure.invoke;

public class Interop {

  private static final String NS = "com.coldcore.misc.clj.interop";

  
  public static Object synchronizeOn(Object obj, Object fn) {
    synchronized (obj) {
      return invoke(NS, "execute-fn", fn);
    }
  }

}