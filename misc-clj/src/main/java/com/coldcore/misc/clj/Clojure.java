package com.coldcore.misc.clj;

import clojure.lang.RT;
import clojure.lang.Var;

public class Clojure {

  static {
    try {
      RT.load("com/coldcore/misc/clj/boot");
    } catch (Throwable e) {
      if (e instanceof RuntimeException) throw (RuntimeException) e;
      throw new ClojureException(e);
    }
  }


  public static Var var(String ns, String fn) throws Exception {
    RT.var("com.coldcore.misc.clj.boot", "load-ns").invoke(ns);
    return RT.var(ns, fn);
  }


  public static Object invoke(String ns, String fn) {
    try {
      return var(ns, fn).invoke();
    } catch (Throwable e) {
      if (e instanceof RuntimeException) throw (RuntimeException) e;
      throw new ClojureException(e);
    }
  }


  public static Object invoke(String ns, String fn, Object arg1) {
    try {
      return var(ns, fn).invoke(arg1);
    } catch (Throwable e) {
      if (e instanceof RuntimeException) throw (RuntimeException) e;
      throw new ClojureException(e);
    }
  }


  public static Object invoke(String ns, String fn, Object arg1, Object arg2) {
    try {
      return var(ns, fn).invoke(arg1, arg2);
    } catch (Throwable e) {
      if (e instanceof RuntimeException) throw (RuntimeException) e;
      throw new ClojureException(e);
    }
  }


  public static Object invoke(String ns, String fn, Object arg1, Object arg2, Object arg3) {
    try {
      return var(ns, fn).invoke(arg1, arg2, arg3);
    } catch (Throwable e) {
      if (e instanceof RuntimeException) throw (RuntimeException) e;
      throw new ClojureException(e);
    }
  }


  public static Object invoke(String ns, String fn, Object arg1, Object arg2, Object arg3, Object arg4) {
    try {
      return var(ns, fn).invoke(arg1, arg2, arg3, arg4);
    } catch (Throwable e) {
      if (e instanceof RuntimeException) throw (RuntimeException) e;
      throw new ClojureException(e);
    }
  }


  public static Object invoke(String ns, String fn, Object arg1, Object arg2, Object arg3, Object arg4, Object arg5) {
    try {
      return var(ns, fn).invoke(arg1, arg2, arg3, arg4, arg5);
    } catch (Throwable e) {
      if (e instanceof RuntimeException) throw (RuntimeException) e;
      throw new ClojureException(e);
    }
  }


  public static Object invoke(String ns, String fn, Object arg1, Object arg2, Object arg3, Object arg4, Object arg5, Object arg6) {
    try {
      return var(ns, fn).invoke(arg1, arg2, arg3, arg4, arg5, arg6);
    } catch (Throwable e) {
      if (e instanceof RuntimeException) throw (RuntimeException) e;
      throw new ClojureException(e);
    }
  }


  public static Object invoke(String ns, String fn, Object arg1, Object arg2, Object arg3, Object arg4, Object arg5, Object arg6, Object arg7) {
    try {
      return var(ns, fn).invoke(arg1, arg2, arg3, arg4, arg5, arg6, arg7);
    } catch (Throwable e) {
      if (e instanceof RuntimeException) throw (RuntimeException) e;
      throw new ClojureException(e);
    }
  }


  public static Object invoke(String ns, String fn, Object arg1, Object arg2, Object arg3, Object arg4, Object arg5, Object arg6, Object arg7, Object arg8) {
    try {
      return var(ns, fn).invoke(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8);
    } catch (Throwable e) {
      if (e instanceof RuntimeException) throw (RuntimeException) e;
      throw new ClojureException(e);
    }
  }


  public static Object invoke(String ns, String fn, Object arg1, Object arg2, Object arg3, Object arg4, Object arg5, Object arg6, Object arg7, Object arg8, Object arg9) {
    try {
      return var(ns, fn).invoke(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9);
    } catch (Throwable e) {
      if (e instanceof RuntimeException) throw (RuntimeException) e;
      throw new ClojureException(e);
    }
  }

}