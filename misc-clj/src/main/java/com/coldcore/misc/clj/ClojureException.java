package com.coldcore.misc.clj;

public class ClojureException extends RuntimeException {

  public ClojureException(Throwable cause) {
    super(cause);
  }
}
