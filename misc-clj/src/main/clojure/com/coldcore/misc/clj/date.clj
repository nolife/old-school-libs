(ns com.coldcore.misc.clj.date
  (:import (java.util Date Locale GregorianCalendar Calendar)))


(defn midnight
  "Convert a date into midnight. Return a date which is midnight."
  [#^Date date]
    (let [cal (GregorianCalendar.)]
      (doto cal
        (.setTime date)
        (.set Calendar/HOUR_OF_DAY 0)
        (.set Calendar/MINUTE 0)
        (.set Calendar/SECOND 0)
        (.set Calendar/MILLISECOND 0))
      (.getTime cal)))


(defn #^Date add-days
  "Add days to a date. Return a date with added days. Use -days to substract."
  [#^Date date num]
    (let [cal (GregorianCalendar.)]
      (doto cal
        (.setTime date)
        (.add Calendar/DAY_OF_MONTH num))
      (.getTime cal)))


(defn #^Date yesterday
  "Yesterday of a date. Return a date which is yesterday."
  [date] (add-days date -1))


(defn #^Date tomorrow
  "Tomorrow of a date. Return a date which is tomorrow."
  [date] (add-days date 1))
