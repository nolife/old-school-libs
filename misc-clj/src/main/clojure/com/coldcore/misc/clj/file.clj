(ns com.coldcore.misc.clj.file
  (:import
    (java.io File FileInputStream FileOutputStream ByteArrayInputStream ByteArrayOutputStream InputStream
      BufferedInputStream BufferedOutputStream))
  (:use [com.coldcore.misc.clj.stream :only (to-output-stream)]))


(defn read-binary-file
  "Read a file data. Return a byte array."
  [#^File f]
    (with-open [in (BufferedInputStream. (FileInputStream. f))]
      (let [out (ByteArrayOutputStream.)]
        (to-output-stream in out)
        (.toByteArray out))))


(defn #^String read-text-file
  "Read a text file. Return a UTF-8 string."
  [f] (String. #^bytes (read-binary-file f) "UTF-8"))


(defmulti
  #^{:doc "Write a file."}
  write-file (fn [_ x] (if (instance? InputStream x) :stream :array)))

(defmethod write-file :stream [#^File f in]
  (.. f getParentFile (mkdirs))
  (with-open [out (BufferedOutputStream. (FileOutputStream. f))]
    (to-output-stream in out)))

(defmethod write-file :array [#^File f data]
  (write-file f (ByteArrayInputStream. data)))


(defn copy-file
  "Copy a file."
  [#^File src #^File trg]
    (with-open [in (BufferedInputStream. (FileInputStream. src))]
      (write-file trg in)))


(defn delete-file
  "Delete a file."
  [#^File f]
    (if (.isFile f)
      (.delete f)
      (let [ls (.listFiles f)]
        (when (pos? (alength ls)) (dorun (map delete-file ls)))
        (.delete f))))

                              
(defn list-if-file
  "List only files in a path. Return a list of files."
  [#^File f] (filter #(.isFile #^File %) (.listFiles f)))


(defn list-if-directory
  "List only directories in a path. Return a list of directories."
  [#^File f] (filter #(.isDirectory #^File %) (.listFiles f)))
