(ns com.coldcore.misc.clj.html
  (:use
    [clojure.contrib.seq-utils :only (flatten)]
    [clojure.contrib.str-utils :only (str-join)]
    [com.coldcore.misc.clj.string :only (chain-replace trim)]))


(defn- map-to-list
  "Convert a map into a list."
  [x] (map #(list (key %) (val %)) x))


(defn #^String plain-to-html
  "Convert a plain text into HTML.
   Return an HTML with basic and/or additional symbols converted into HTML entities."
  ([text rep]
    (if-not text
      ""
      (let [seq-1 (map-to-list {"&" "&amp;" "<" "&lt;" ">" "&gt;" "\n" "<br>" "\r" "" "\"" "&quot;" "'" "&apos;" "  " " &nbsp;"}) ;map with basic symbols
            seq-2 (map-to-list rep)] ;map with additional symbols
        (chain-replace text (partition 2 (flatten (conj [] seq-1 seq-2)))))))
  ([text] (plain-to-html text {})))


(defn- compact-lb
  "Strip line breakes in a text to a single line break."
  [#^String text]
    (let [arr (.split text "\n")]
      (str-join "\n" (filter seq (map trim arr)))))


(defn #^String plain-to-html-lb
  "Convert a plain text into HTML stripping line breaks to a single and changing basic symbols into entities.
   Use to format a free hand multiline text entered by a user."
  [text] (plain-to-html (compact-lb text)))

