(ns com.coldcore.misc.clj.stream
  (:import (java.io ByteArrayOutputStream InputStream OutputStream)))


(defn to-output-stream
  "Write an input stream into an output stream."
  [#^InputStream in #^OutputStream out]
    (let [b (make-array Byte/TYPE 4096)]
      (loop [i (.read in b)]
        (when-not (neg? i)
          (.write out b 0 i)
          (recur (.read in b))))))


(defn to-byte-array
  "Write an input stream into a byte array."
  [in]
    (let [out (ByteArrayOutputStream.)]
      (to-output-stream in out)
      (.toByteArray out)))

