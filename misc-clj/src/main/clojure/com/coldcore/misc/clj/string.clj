(ns com.coldcore.misc.clj.string
  (:import (java.util.regex Pattern))
  (:use
    [clojure.contrib.str-utils :only (str-join)]
    [clojure.contrib.seq-utils :only (flatten)]))


;; ************** Transforming (no dependencies) **************** ;;


(defn- #^String first-match
  "Used in the regex match method."
  [m] (if (coll? m) (first m) m))


(defn- match
  "Match a first regex in a text. Return a range where a match occurs."
  [#^String regex #^String text flags]
    (let [m (first-match (re-find (. Pattern (compile regex flags)) text))]
      (when m
        (let [ind (.indexOf text m) len (.length m)]
          [ind (+ ind len)]))))


(defn- index-token
  "Create an indexed token with a matched prefix and suffix regex in a text.
   Return a map with a prefix/suffix range."
  [pfx sfx text flags]
    (let [matched-pfx (match pfx text flags) matched-sfx (match sfx text flags)]
      (when (and matched-pfx matched-sfx)
        {:start-pfx (first matched-pfx) :end-pfx (second matched-pfx)
         :start-sfx (first matched-sfx) :end-sfx (second matched-sfx)})))


(defn- token
  "Operate on an indexed token to extract a token value from a text laying between its prefix and suffix."
  [indexed-token text] (subs text (indexed-token :end-pfx) (indexed-token :start-sfx)))


(defn- token-pfx
  "Operate on an indexed token to extract a prefix value from a text."
  [indexed-token text] (subs text (indexed-token :start-pfx) (indexed-token :end-pfx)))


(defn- token-sfx
  "Operate on an indexed token to extract a suffix value from a text."
  [indexed-token text] (subs text (indexed-token :start-sfx) (indexed-token :end-sfx)))


(defn find-tokens
  "Find all tokens in a text with a matched prefix and suffix.
   Return a list of tokens."
  ([pfx sfx text flags]
    (loop [result [] text text]
      (let [indexed-token (index-token pfx sfx text flags)]
        (if-not indexed-token
          result
          (recur (conj result (token indexed-token text)) (subs text (indexed-token :end-sfx)))))))
  ([pfx sfx text] (find-tokens pfx sfx text 0)))


(defn find-tokens-ci
  "Find all tokens in a text with a matched prefix and suffix (case insensitive search).
   Return a list of tokens."
  [pfx sfx text] (find-tokens pfx sfx text Pattern/CASE_INSENSITIVE))


(defn- tokens-pfx-sfx
  "Identify all prefixes and suffixes in a text based on regex. Return a list of prefixes and suffixes."
  [pfx sfx text flags]
    (loop [result [] text text]
      (let [indexed-token (index-token pfx sfx text flags)]
        (if-not indexed-token
          result
          (recur (conj result (token-pfx indexed-token text) (token-sfx indexed-token text)) (subs text (indexed-token :end-sfx)))))))


(defn #^String chain-replace
  "Replace strings in a text based on a sequence containing pairs (replace a key with a value for each pair).
   abcdef [[a b] [c d] [e f]] -> bbddff"
  [#^String text x] (reduce #(.replace #^String %1 (str (first %2)) (str (second %2))) text x))


(defn- cross-seq
  "Create a cross product from a map containing all combinations of prefixes and suffixes split into pairs."
  [rep pfx-sfx]
    (partition 2 (flatten
      (for [pfx-sfx-seq (partition 2 pfx-sfx) rep-seq rep]
        [(str (first pfx-sfx-seq) (first rep-seq) (second pfx-sfx-seq)) (second rep-seq)]))))


(defn #^String replace-tokens
  "Replace tokens in a text laying between a prefix and suffix regex with values provided in a map.
   Look for a map key surrounded by a prefix and suffix and change it with a map value."
  ([pfx sfx text rep flags]
    (let [pfx-sfx (tokens-pfx-sfx pfx sfx text flags)
          rep-seq (cross-seq rep pfx-sfx)]
      (if (empty? rep-seq) text (chain-replace text rep-seq))))
  ([pfx sfx text rep] (replace-tokens pfx sfx text rep 0)))


(defn #^String replace-tokens-ci
  "Replace tokens as in the replace-tokens method (case insensitive prefix/suffix search)."
  [pfx sfx text rep] (replace-tokens pfx sfx text rep Pattern/CASE_INSENSITIVE))


(defn- keyval-pair
  "Split a string into a key and value. Return a [key val] pair."
  [#^String prm] [(subs prm 0 (.indexOf prm ":")) (subs prm (inc (.indexOf prm ":")))])


(defn- keyval-map
  "Create a map from [key val] pairs."
  [pairs] (if (empty? pairs) {} (reduce #(assoc %1 (first %2) (second %2)) {} pairs)))


(defn #^String parametrize-text
  "Substitude parameters in a text with values provided as a sequence (param:value param:value)."
  [text prms] (when text (replace-tokens-ci "\\{" "\\}" text (keyval-map (map keyval-pair prms)))))


(defn #^String parametrize
  "Substitude parameters in a text with provided values (param:value param:value).
   Welcome {name} name:Katie -> Welcome Katie"
  [text & more] (parametrize-text text (into [] more)))


;; ************** Trimming (no dependencies) **************** ;;


(defn #^String trim
  "Trim a string."
  [s] (.trim (str s)))


(defn #^String trimu
  "Trim and upper case a string."
  [s] (.toUpperCase (trim s)))


(defn #^String triml
  "Trim and lower case a string."
  [s] (.toLowerCase (trim s)))


(defn #^String trimm
  "Trim a string removing redundant line feeds."
  [s] (str-join "\n" (filter #(pos? (.length #^String %)) (map trim (.split (str s) "\n")))))


(defn #^String trim-cut
  "Trim a string and cut if it exceeds maximum lenght."
  [s len]
    (let [str (trim s)]
      (if (> (.length str) len) (trim (subs str 0 len)) str)))


;; ************** Parsing (depends on Transforming and Trimming) **************** ;;


(defn #^Boolean is-empty?
  "Check is a string is empty (a string does not contain valid chars)."
  [#^String s] (or (nil? s) (zero? (.length s))))


(defn #^Boolean is-not-empty?
  "Check is a string is not empty (a string contains valid chars)."
  [s] (not (is-empty? s)))


(defn #^Boolean is-blank?
  "Check is a string is blank (a trimmed string does not contain valid chars).
   Use to check a free hand text entered by a user."
  [s] (is-empty? (trim s)))


(defn #^Boolean is-not-blank?
  "Check is a string is not blank (a trimmed string contains valid chars).
   Use to check a free hand text entered by a user."
  [s] (not (is-blank? s)))


(defn #^Long to-long
  "Convert a string into a number. Return a number or 0 or default is specified."
  ([s] (to-long s (long 0)))
  ([s d] (try (Long/parseLong (trim s)) (catch Throwable e (long d)))))


(defn #^Integer to-int
  "Convert a string into a number. Return a number or 0 or default is specified."
  ([s] (to-int s 0))
  ([s d] (try (Integer/parseInt (trim s)) (catch Throwable e d))))


(defn #^Boolean is-true?
  "Convert a string into a boolean (1 - true, 0 - false). Return a boolean or false or default is specified."
  ([s] (is-true? s false))
  ([s d] (= (to-long s (if d 1 0)) 1)))


(defmulti
  #^{:doc
  "Convert a string with precision into a whole number or a whole number back into a string with precision.
   ['123.56' 2] -> 12356   ['123.56' 4] -> 1235600
   [12356 2] -> '123.56'   [1235600 4] -> '123.56'"}
  to-x-prec (fn [x _] (class x)))

(defmethod #^Long to-x-prec String [#^String s prec]
    (let [mult (long (Math/pow 10 prec))
          zero (subs (str mult) 1)
          neg (.startsWith s "-") pos (.startsWith s "+")
          val-0 (.replace (str "0" (if (or neg pos) (subs s 1) s)) "," ".")
          val (str (if (.contains val-0 ".") val-0 (str val-0 ".")) zero)
          arr (.split val "\\.")
          sign (if neg -1 1)]
      (* sign (long (+
                      (* (Long/parseLong (first arr)) mult)
                      (Long/parseLong (subs (second arr) 0 (.length #^String zero))))))))

(defmethod #^String to-x-prec Number [n prec]
    (let [mult (long (Math/pow 10 prec))
          z (if (neg? n) (* n -1) n)
          a (long (/ z mult))
          b (long (- z (* a mult)))
          d (apply str (map (fn [_] "0") (range 1 (- (.length (str mult)) (.length (str b))))))
          s (str a "." d b)
          sign (if (neg? n) "-" "")]
      (str sign s)))


(defn to-x100
  "Convert a string or number with a precision of 2.
   ['123.56' 2] -> 12356   [12356 2] -> '123.56'"
  [x] (to-x-prec x 2))


(defn to-xLatLng [x]
  "Convert a latitude or longiture string or number with maximum precision of 16.
   ['123.56' 2] -> 12356   [12356 2] -> '123.56'"
  (to-x-prec x 16))


(defn- rep-seq
  "Convert a string into a sequence of pairs.
   ',:+-' -> [: ,] [+ ,] [- ,]"
  [sp] (rest (map (fn [x] [x (first sp)]) sp)))


(defn parse-csv
  "Parse a comma separated values. Return a sequence of trimmed strings. Inputs: text and a separator string.
   ['foo, bar; goo' ',;' -> [foo bar goo]"
  ([s sp]
    (let [s-1 (chain-replace s (rep-seq sp)) ;convert text to a single separator (foo, bar; goo -> foo, bar, goo)
          arr (.split s-1 (Pattern/quote (str (first sp))))] ;split text using a separator (as regex)  
      (filter seq (map trim arr))))
  ([s] (parse-csv s ",;")))


(defn parse-csv-map
  "Parse a comma separated map values. Return a map with trimmed keys and values. Inputs: text and a separator string.
   ['foo=F, bar=B; goo=G' ',;' -> {foo A, bar B, goo G}"
  ([s sp]
    (reduce #(let [i (.indexOf #^String %2 "=")]
               (assoc %1 (trim (subs %2 0 i)) (trim (subs %2 (inc i)))))
      {} (parse-csv s sp)))
  ([s] (parse-csv-map s ",;")))





;; ************** Util (depends on above) **************** ;;

(defmacro non-empty
  "Return the first not empty value or nil."
  ([] nil)
  ([x] x)
  ([x & next]
    `(if (is-not-empty? ~x) ~x (non-empty ~@next))))
