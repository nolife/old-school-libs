(ns com.coldcore.misc.clj.other
  (:import
    (java.net URL)
    (java.io StringWriter PrintWriter BufferedReader InputStreamReader))
  (:use [com.coldcore.misc.clj.string :only (is-not-empty? trim triml)]))


(defn- append-trace
  "Append a stack trace to a string and descent into its cause."
  [#^StringBuilder sb error]
    (loop [#^Throwable e error]
      (when e
        (let [msg (.getMessage e)
              sw (StringWriter.)]
          (when (pos? (.length sb)) (.. sb (append "Caused by:\n")))
          (when (is-not-empty? msg) (.. sb (append msg) (append "\n"))) ;append an optional message
          (.printStackTrace e (PrintWriter. sw)) ;print a trace
          (.. sb (append sw) (append "\n"))) ;append a trace
        (recur (.getCause e)))))


(defn #^String print-stack-trace
  "Construct a string containing a full stack trace."
  [error]
    (let [sb (StringBuilder.)]
      (append-trace sb error)
      (.toString sb)))


(defn #^String fix-url
  "Format a string as a URL. Use to format free hand text entered by a user."
  [url]
    (let [url-0 (trim url)
          url-1 (if (or (.startsWith url-0 "http://") (.startsWith url-0 "https://")) url-0 (str "http://" url-0))
          url-2 (if (.endsWith url-1 "/") (subs url-1 0 (dec (.length url-1))) url-1)]
      (if (< (.length #^String url-2) 10) "" url-2)))


(defn #^String fetch-url
  "Read a URL as a text. Return UTF-8 URL content."
  [url]
    (let [u (URL. url)]
      (with-open [stream (.openStream u)]
        (let [buf (BufferedReader. (InputStreamReader. stream "UTF-8"))]
          (apply str (line-seq buf))))))
