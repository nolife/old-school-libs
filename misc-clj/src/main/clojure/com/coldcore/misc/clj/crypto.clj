(ns com.coldcore.misc.clj.crypto
  (:import
    (java.security MessageDigest)
    (java.io InputStream BufferedInputStream FileInputStream ByteArrayInputStream File)))


(defn- update-digest
  "Call the update method on a digest until a stream is exhausted."
  [#^InputStream in #^MessageDigest md]
    (do
      (.reset md)
      (let [b (make-array Byte/TYPE 1024)]
        (loop [i (.read in b)]
          (when-not (neg? i)
            (.update md b 0 i)
            (recur (.read in b)))))))


(defmulti
  #^{:doc "Create a digest."}
  digest (fn [x _] (class x)))

(defmethod #^String digest InputStream [in alg]
  (let [md (MessageDigest/getInstance alg)]
    (update-digest in md)
    (apply str (map #(Integer/toHexString (bit-and 0xFF %)) (.digest md))))) ;convert a byte array into a string

(defmethod #^String digest File [#^File f alg]
  (with-open [in (BufferedInputStream. (FileInputStream. f))]
    (digest in alg)))

(defmethod #^String digest String [#^String s alg]
  (digest (ByteArrayInputStream. (.getBytes s "UTF-8")) alg))
