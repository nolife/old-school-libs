(ns com.coldcore.misc.clj.reflect
  (:import (java.lang.reflect Field Method))
  (:use [clojure.contrib.seq-utils :only (flatten)]))


(defn- named
  "Convert 'x' to String"
  [x]
    (if (keyword? x)
      (name x)
      (if (or (instance? Field x) (instance? Method x)) (.getName x) x)))


(defn- getter
  "xz -> getXz   :xy -> getXz   etc."
  [x]
    (let [s (named x)]
      (str "get" (.toUpperCase #^String (subs s 0 1)) (subs s 1))))


(defn- setter
  "xz -> setXz   :xy -> setXz   etc."
  [x]
    (let [s (named x)]
      (str "set" (.toUpperCase #^String (subs s 0 1)) (subs s 1))))


(defn filter-class-fields
  "Find fields in a class or its parents by a supplied filter."
  [#^Class clazz filter-fn]
    (flatten (filter identity
      (conj
        (when (.getSuperclass clazz) (filter-class-fields (.getSuperclass clazz) filter-fn))
        (filter filter-fn (.getDeclaredFields clazz))))))


(defn filter-class-methods
  "Find methods in a class by a supplied filter."
  [#^Class clazz filter-fn] (filter filter-fn (.getMethods clazz)))


(defn fields-by-annotation
  "Find fields in a class or its parents by an annotation."
  [clazz clazz-an] (filter-class-fields clazz #(.isAnnotationPresent #^Field % clazz-an)))


(defn #^Field field-by-annotation
  "Find a field in a class or its parents by the field's annotation."
  [clazz clazz-an] (first (fields-by-annotation clazz clazz-an)))


(defn #^Field field-by-name
  "Find a field in a class or its parents by the field's name. 'field' is a String / Keyword / Field."
  [clazz field] (first (filter-class-fields clazz #(= (named field) (.getName %)))))


(defn methods-by-annotation
  "Find methods in a class by an annotation."
  [clazz clazz-an] (filter-class-methods clazz #(.isAnnotationPresent #^Method % clazz-an)))


(defn #^Method method-by-annotation
  "Find a method in a class by the method's annotation."
  [clazz clazz-an] (first (methods-by-annotation clazz clazz-an)))


(defn #^Method method-by-name
  "Find a method in a class by the method's name. 'method' is a String / Keyword / Method."
  [clazz method] (first (filter-class-methods clazz #(= (named method) (.getName %)))))


(defn invoke-method
  "Call a method of an object. 'method' is a String / Keyword / Method."
  [obj method & args]
    (let [mth-name (if (instance? String method) method (named method))
          mth (try
                (.getDeclaredMethod (class obj) mth-name (into-array Class (map class args)))
                (catch NoSuchMethodException e (method-by-name (class obj) mth-name)))] ;default method
      (.invoke #^Method mth obj (to-array args))))


(defmulti
  #^{:doc
  "Get a property's value of a bean or a map. 'field' is a String / Keyword."}
  bean-val (fn [x _] (if (map? x) :map :obj)))

(defmethod bean-val :map [m field]
  (m field))

(defmethod bean-val :obj [obj field]
  (invoke-method obj (getter field)))


(defn field-value
  "Invoke a getter on a field and return its result. 'field' is a String / Keyword / Field."
  [obj field] (invoke-method obj (getter field)))

(defn reset-field-value!
  "Invoke a setter on a field. 'field' is a String / Keyword / Field."
  [obj field value] (invoke-method obj (setter field) value))


(defmulti
  #^{:doc
  "Copy fields between beans and maps. 'fields' are String / Keyword."}
  bean-copy (fn [x y & _] (if (map? x) :map_obj (if (map? y) :obj_map :obj_obj))))

(defmethod bean-copy :map_obj
  ([m obj] (doseq [e m] (reset-field-value! obj (key e) (val e))))
  ([m obj & fields] (doseq [f fields] (reset-field-value! obj f (m f)))))

(defmethod bean-copy :obj_map [obj m & fields]
  (let [props (bean obj)] ;returns bean properties {:key value ...}
    (reduce #(let [k (if (keyword? %2) %2 (keyword %2))] ;ensure that a key is a Keyword
               (assoc %1 k (props k))) m fields)))

(defmethod bean-copy :obj_obj [obj trg & fields]
  (bean-copy (apply bean-copy obj {} fields) trg))
