(ns com.coldcore.misc.clj.generator
  (:use [clojure.contrib.fcase :only (case)]))


(defn- random-int
  "Return a random integer in a range [x .. y]."
  [x y] (+ x (rand-int (- (inc y) x))))


(defn- random-char
  "Calculate a random char."
  []
    (case (rand-int 3)
      0 (char (random-int 48 57))
      1 (char (random-int 65 90))
      (char (random-int 97 122))))


(defn #^String gen-id
  "Calculate a random ID containing characters and numbers. Input: fixed lenght or lenght range."
  ([len] (gen-id len len))
  ([min max] (apply str (map (fn [_] (random-char)) (range 0 (random-int min max))))))


(defn #^String gen-idn
  "Calculate a random ID containing numbers. Input: fixed lenght or lenght range."
  ([len] (gen-idn len len))
  ([min max] (apply str (map (fn [_] (rand-int 10)) (range 0 (random-int min max))))))
