(ns com.coldcore.misc.clj.interop
  (:import (com.coldcore.misc.clj Interop)))


(defn execute-fn
  "Called from Java blocks such as 'synchronized' to execute a callback function specified by a script."
  [f] (f))


(defn synchronize-on
  "Execute a callback function synchronizing on an object."
  [obj callback-fn] (Interop/synchronizeOn obj callback-fn))

