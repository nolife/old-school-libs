(ns com.coldcore.misc.clj.boot)


; (set! *warn-on-reflection* true) ; enable only for tests


(def m-require (memoize require))

(defn resolve-sym
  "Resolve NS (memorized) and return var."
  [ns-sym sym]
    (m-require ns-sym)
    (ns-resolve ns-sym sym))


(defn load-ns
  "Load NS if not yet loaded."
  [ns] (resolve-sym (symbol ns) 'dummy))
