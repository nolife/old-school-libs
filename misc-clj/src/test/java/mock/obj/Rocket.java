package mock.obj;

public class Rocket {

  private String name;
  private Integer target;
  @Parameter public Integer weight;
  @Parameter public Integer fuel;
  public String engine;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Integer getTarget() {
    return target;
  }

  public void setTarget(Integer target) {
    this.target = target;
  }

  @Launcher
  public void launcher() {
  }

  @Launcher
  public void exploder() {
  }

  public String param(String param) {
    return name+param;
  }
  
  public Integer param(Integer param) {
    return target+param;
  }

  public void configure(String name) {
    this.name = name;
  }

  public void configure(String name, Integer target) {
    this.name = name;
    this.target = target;
  }
}