(ns string-test
  (:use
    [com.coldcore.misc.clj.string]
    [clojure.test]))


(deftest test-parametrize
  (let [input "The {amount} on {date}"]
    (is (= "The 100 on 200" (parametrize input "amount:100" "date:200")))
    (is (= "The 100 on {date}" (parametrize input "amount:100")))
    (is (= "The 100 on 200" (parametrize-text input ["amount:100" "date:200"])))
    (is (= "The 100 on 200" (parametrize-text input (into-array String ["amount:100" "date:200"]))))
    (is (= "The 100 on {date}" (parametrize-text input ["amount:100"])))
    (is (= nil (parametrize nil "amount:100" "date:200")))
    (is (= nil (parametrize-text nil ["amount:100" "date:200"])))
    (is (= input (parametrize-text input [])))
    (is (= input (parametrize input)))
    (is (= input (parametrize input "my:void")))))


(deftest test-parse-and-trim
  (is (= "the line\nwith breaks\nand trailings" (trimm " the line\n   with breaks  \r\n\r\n  and trailings  ")))
  (is (= "" (trimm nil)))
  (is (= "no trail" (trim "   no trail   ")))
  (is (= "" (trim nil)))
  (is (= "ALL UPPER" (trimu " All upper ")))
  (is (= "all lower" (triml " All LOWER ")))
  (is (= "some" (trim-cut " some cutted ... ooouno " 5)))
  (is (= "some c" (trim-cut " some cutted ... ooouno " 6)))
  (is (true? (is-true? " 1 ")))
  (is (false? (is-true? " 0 ")))
  (is (false? (is-true? " 0 " true)))
  (is (false? (is-true? " go ")))
  (is (true? (is-true? " go " true)))
  (is (false? (is-true? " go " false)))
  (is (false? (is-true? nil)))
  (is (false? (is-true? nil false)))
  (is (true? (is-true? nil true))))


(deftest test-parse-csv
  (let [input-1 "  key1=val1; key2 = val2 ,,  ;, key3 = val3 , key4 = val4 "
        input-2 "  key1=val1; key2 = val2 ,, val2n ; key3 = val3,,,val3n=X; key4 = val4 "]
    (is (= ["key1=val1" "key2 = val2" "key3 = val3" "key4 = val4"] (parse-csv input-1)))
    (is (= ["key1" "val1" "key2" "val2" "val2n" "key3" "val3" "val3n" "X" "key4" "val4"] (parse-csv input-2 "=,;")))
    (is (= {"key1" "val1" "key2" "val2" "key3" "val3" "key4" "val4"} (parse-csv-map input-1)))
    (is (= {"key1" "val1" "key2" "val2 ,, val2n" "key3" "val3,,,val3n=X" "key4" "val4"} (parse-csv-map input-2 ";")))
    (is (= {} (parse-csv-map "")))
    (is (= {"key" ""} (parse-csv-map "key=")))))


(deftest test-to-x
  (is (= 8000 (to-x100 "80")))
  (is (= "80.00" (to-x100 8000)))
  (is (= 7090 (to-x100 "70,9")))
  (is (= "70.90" (to-x100 7090)))
  (is (= 6095 (to-x100 "60.95")))
  (is (= "60.95" (to-x100 6095)))
  (is (= 5095 (to-x100 "50.959")))
  (is (= "50.95" (to-x100 5095)))
  (is (= 0 (to-x100 "0")))
  (is (= "0.00" (to-x100 0)))
  (is (= 40 (to-x100 ".4")))
  (is (= "0.40" (to-x100 40)))
  (is (= 0 (to-x100 "")))
  (is (= "0.00" (to-x100 0)))
  (is (= -8000 (to-x100 "-80")))
  (is (= "-80.00" (to-x100 -8000)))
  (is (= -40 (to-x100 "-0.4")))
  (is (= "-0.40" (to-x100 -40))))


(deftest test-empty-and-blank
  (is (true? (is-empty? "")))
  (is (false? (is-empty? " ")))
  (is (false? (is-empty? "a")))
  (is (true? (is-blank? "")))
  (is (true? (is-blank? " ")))
  (is (false? (is-blank? "a")))
  (is (false? (is-blank? "  a"))))


(deftest test-to-number
  (is (= 10 (to-long "10")))
  (is (= 10 (to-long " 10 ")))
  (is (= 10 (to-long " x " 10)))
  (is (= 0 (to-long " x ")))
  (is (= 10 (to-int "10")))
  (is (= 10 (to-int " 10 ")))
  (is (= 10 (to-int " x " 10)))
  (is (= 0 (to-int " x "))))


(deftest test-non-empty
  (is (= "a" (non-empty "" "a" "")))
  (is (= nil (non-empty "" "" (when false "a"))))
  (is (= "b" (non-empty "" "" (or nil (non-empty "" nil nil "" "b" "c" "d"))))))



