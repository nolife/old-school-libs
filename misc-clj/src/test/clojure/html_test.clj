(ns html-test
  (:use
    [com.coldcore.misc.clj.html]
    [clojure.test]))


(deftest test-plain-to-html
  (let [input-plain "   this # is \r\n next # line <p>T_T</p> \n\n\n   t  a  g   \n  \n'Q_Q\" &  #?!"]
    (is (= " &nbsp; this # is <br> next # line &lt;p&gt;T_T&lt;/p&gt; <br><br><br> &nbsp; t &nbsp;a &nbsp;g &nbsp; <br> &nbsp;<br>&apos;Q_Q&quot; &amp; &nbsp;#?!" (plain-to-html input-plain)))
    (is (= " &nbsp; this &hash; is <br> next &hash; line &lt;p&gt;T_T&lt;/p&gt; <br><br><br> &nbsp; t &nbsp;a &nbsp;g &nbsp; <br> &nbsp;<br>&apos;Q_Q&quot; &amp; &nbsp;&hash;?!" (plain-to-html input-plain {"#" "&hash;"})))
    (is (= " &nbsp; this # is \n next # line &lt;p&gt;T_T&lt;/p&gt; \n\n\n &nbsp; t &nbsp;a &nbsp;g &nbsp; \n &nbsp;\n&apos;Q_Q&quot; &amp; &nbsp;#?!" (plain-to-html input-plain {"<br>" "\n"})))
    (is (= "" (plain-to-html nil)))
    (is (= "this # is<br>next # line &lt;p&gt;T_T&lt;/p&gt;<br>t &nbsp;a &nbsp;g<br>&apos;Q_Q&quot; &amp; &nbsp;#?!" (plain-to-html-lb input-plain)))))


