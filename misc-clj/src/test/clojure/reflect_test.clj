(ns reflect-test
  (:import
    (mock.obj MyBean Rocket Launcher Parameter))
  (:use
    [com.coldcore.misc.clj.reflect]
    [clojure.test]))


(deftest test-bean-copy-map-obj
  (let [obj (MyBean.)]
    (doto obj (.setName "bean") (.setTarget 1))
    (is (= {:target 1 :name "bean" :hash 12} (bean-copy obj {:hash 12} :name :target)))
    (bean-copy {:target 2 :name  "bean-2"} obj)
    (is (= (.getName obj) "bean-2"))
    (is (= (.getTarget obj) 2))
    (bean-copy {:target 3 :name  "bean-3"} obj :target)
    (is (= (.getName obj) "bean-2"))
    (is (= (.getTarget obj) 3))))


(deftest test-bean-copy-map-obj-2
  (let [obj (MyBean.)]
    (doto obj (.setName "bean") (.setTarget 1))
    (is (= {:target 1 :name "bean" :hash 12} (bean-copy obj {:hash 12} "name" "target")))
    (bean-copy {"target" 2 "name"  "bean-2"} obj)
    (is (= (.getName obj) "bean-2"))
    (is (= (.getTarget obj) 2))
    (bean-copy {"target" 3 "name"  "bean-3"} obj "target")
    (is (= (.getName obj) "bean-2"))
    (is (= (.getTarget obj) 3))))


(deftest test-bean-copy-obj-obj
  (let [obj (MyBean.) my-obj (MyBean.)]
    (doto obj (.setName "bean") (.setTarget 1))
    (doto my-obj (.setName "my-bean") (.setTarget 2))
    (bean-copy my-obj obj :name :target)
    (is (= (.getName obj) "my-bean"))
    (is (= (.getTarget obj) 2))))


(deftest test-bean-copy-obj-obj-2
  (let [obj (MyBean.) my-obj (MyBean.)]
    (doto obj (.setName "bean") (.setTarget 1))
    (doto my-obj (.setName "my-bean") (.setTarget 2))
    (bean-copy my-obj obj "name" "target")
    (is (= (.getName obj) "my-bean"))
    (is (= (.getTarget obj) 2))))


(deftest test-bean-val
  (let [obj (MyBean.) m {:name "my-bean" :target 2}]
    (doto obj (.setName "bean") (.setTarget 1))
    (is (= "my-bean" (bean-val m :name)))
    (is (= 2 (bean-val m :target)))
    (is (= "bean" (bean-val obj :name)))
    (is (= 1 (bean-val obj :target)))))


(deftest test-bean-val-2
  (let [obj (MyBean.) m {"name" "my-bean" "target" 2}]
    (doto obj (.setName "bean") (.setTarget 1))
    (is (= "my-bean" (bean-val m "name")))
    (is (= 2 (bean-val m "target")))
    (is (= "bean" (bean-val obj "name")))
    (is (= 1 (bean-val obj "target")))))


(deftest test-invoke-method
  (let [r (Rocket.)]
    (doto r (.setName "rocket") (.setTarget 1))
    (is (= "rocket" (invoke-method r "getName")))
    (is (= 1 (invoke-method r :getTarget)))
    (invoke-method r "configure" "missile")
    (is (= "missile" (invoke-method r "getName")))
    (is (= 1 (invoke-method r :getTarget)))
    (invoke-method r :configure "a-bomb" 2)
    (is (= "a-bomb" (invoke-method r "getName")))
    (is (= 2 (invoke-method r :getTarget)))
    (is (= "a-bomb-oo" (invoke-method r "param" "-oo")))
    (is (= 5 (invoke-method r :param 3)))))


(deftest test-reset-field-value!
  (let [obj (MyBean.)]
    (doto obj (.setName "bean") (.setTarget 1))
    (reset-field-value! obj "name" "my-bean")
    (reset-field-value! obj :target 2)
    (is (= (.getName obj) "my-bean"))
    (is (= (.getTarget obj) 2))))


(deftest test-find-methods-and-fields
  (is (= "weight" (.getName (field-by-name Rocket "weight"))))
  (is (= "weight" (.getName (field-by-name Rocket :weight))))
  (is (= "getName" (.getName (method-by-name Rocket "getName"))))
  (is (= "getName" (.getName (method-by-name Rocket :getName))))
  (is (= 2 (count (methods-by-annotation Rocket Launcher))))
  (is (not (nil? (method-by-annotation Rocket Launcher))))
  (is (= 2 (count (fields-by-annotation Rocket Parameter))))
  (is (not (nil? (field-by-annotation Rocket Parameter)))))
