(ns other-test
  (:use
    [com.coldcore.misc.clj.other]
    [clojure.test]))


(deftest test-fix-url
  (is (= "http://some.com/hello.htm?id=2" (fix-url " some.com/hello.htm?id=2  ")))
  (is (= "https://some.com" (fix-url " https://some.com/ ")))
  (is (= "http://mydomain" (fix-url "mydomain")))
  (is (= "" (fix-url "Z")))
  (is (= "" (fix-url nil))))


